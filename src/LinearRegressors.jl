#=
 * File: LinearRegressors.jl
 * Project: src
 * File Created: Monday, 11th November 2019 6:42:28 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 12:52:51 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module LinearRegressors
import LinearAlgebra
using MetricTools

using ..MetricRegressors

export LinearRegressor

#region linear
"""
A regressor consisting of a set of linear approximations
"""
mutable struct LinearRegressor <: Regressor
    X::Array{Point}
    Y::Array{<:Real}
    approx::Array{<:Real}
    function LinearRegressor()
        return new(Real[], Real[], Real[])
    end
end

"""
Predicts a point using a linear apporixmation
"""
MetricRegressors.predict(regressor::LinearRegressor, X::Array{Point}) = 
    [predict(regressor, x) for x in X]
MetricRegressors.predict(regressor::LinearRegressor, x::Point)::Real =
    LinearAlgebra.dot([1, x...], regressor.approx)

function MetricRegressors.train!(
        regressor::LinearRegressor,
        X::Array{Point},
        Y::Array{<:Real})
    
    append!(regressor.X, X)
    append!(regressor.Y, Y)
    regressor.approx = linearapprox(regressor.X, regressor.Y)
end
    
"""
Constructs a linear approximation of a function using a list of sample points
"""
function linearapprox(X::Array{Point}, func::Function)::Array{<:Real}
    Y::Array{<:Real} = [func(x) for x in X]
    return linearapprox(X, Y)
end

"""
Constructs a linear approximation for list of points and their values
"""
function linearapprox(X::Array{Point}, Y::Array{<:Real})::Array{<:Real}
    rhs::Array{Float64} = convert(Array{Float64}, Y)
    getv::Function = (i) -> (x) -> (x[i])
    vals::Array{Array{Float64}} = [getv(i).(X) for i in 1:length(first(X))]
    matrix::Matrix{Float64} = hcat(fill(1, length(X)), vals...)
    result::Array{Float64} = Float64[]
    try
        result = matrix \ rhs
    catch e
        println("Error calculating linear approx for: ")
        println("matrix = $matrix")
        println("rhs = $rhs")
    end
    return result
end

function MetricRegressors.getnumparams(regressor::LinearRegressor)::Integer
    length(regressor.approx)
end
end
