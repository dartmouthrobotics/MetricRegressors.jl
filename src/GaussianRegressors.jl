#=
 * File: GaussianRegressors.jl
 * Project: src
 * File Created: Monday, 11th November 2019 6:41:58 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 12:56:27 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module GaussianRegressors

import GaussianProcesses
using MetricTools
using ..MetricRegressors

export GaussianRegressor

mutable struct GaussianRegressor <: Regressor
    gp::GaussianProcesses.GPBase
    function GaussianRegressor(;
            mean::GaussianProcesses.Mean = GaussianProcesses.MeanZero(),
            kernel::GaussianProcesses.Kernel = GaussianProcesses.SE(0.0, 0.0),
            logNoise::Real = -2.0)
        gp = GaussianProcesses.GPE(;mean=mean, kernel=kernel, logNoise=logNoise)
        return new(gp)
    end
end

function MetricRegressors.train!(regressor::GaussianRegressor, X::Array{Point}, Y::Array{<:Real})
    X1::Matrix{<:Real} = hcat(X...)
    
    if isempty(regressor.gp.x)
        regressor.gp.x = X1
        regressor.gp.y = Y
    else
        X = cat(regressor.gp.x, X1; dims=2)
        Y = cat(regressor.gp.y, Y; dims=1)
    end

    GaussianProcesses.fit!(regressor.gp, X1, Y)
end

function MetricRegressors.predict(regressor::GaussianRegressor, X::Array{Point})::Array{<:Real}
    X1::Matrix{<:Real} = hcat(X...)
    means, variances = GaussianProcesses.predict_f(regressor.gp, X1)
    return means
end

function MetricRegressors.getnumparams(regressor::GaussianRegressor)::Integer
    length(regressor.gp.x)^2
end
end
