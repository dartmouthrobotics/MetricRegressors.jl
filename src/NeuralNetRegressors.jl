#=
 * File: NeuralNetRegressors.jl
 * Project: src
 * File Created: Monday, 11th November 2019 6:42:15 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 12:57:42 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module NeuralNetRegressors

using Flux, ProgressMeter
using MetricTools

using ..MetricRegressors

export NeuralNetRegressor

# Custom error functions
maxse(ŷ, y) = maximum((ŷ .- y).^2)
mqe(ŷ, y) = sum((ŷ .- y).^4) * 1 // length(y)

mutable struct NeuralNetRegressor <: Regressor
    model::Flux.Chain
    lossf::Function
    data::Array{Tuple{Array{<:Real}, Array{<:Real}}}  # [(x1, y1), ...]
    opt::ADAM
    epochs::Integer
    function NeuralNetRegressor(;
            model=Chain(
                f64(Dense(2, 2, sigmoid)),
                f64(Dense(2, 2)),
                f64(Dense(2, 1)))::Flux.Chain,
            lossf=(x, y) -> mqe(model(x), y),
            epochs=5::Integer)
        return new(model, lossf, [], ADAM(), epochs)
    end
end

function MetricRegressors.train!(regressor::NeuralNetRegressor, X::Array{Point}, Y::Array{<:Real})
    regressor.data = vcat(regressor.data, [(X[i], [Y[i]]) for i in eachindex(X)])
    @showprogress for i in 1:regressor.epochs
        Flux.train!(
            regressor.lossf,
            Flux.params(regressor.model),
            regressor.data,
            regressor.opt)
    end
end

function MetricRegressors.predict(regressor::NeuralNetRegressor, X::Array{Point})::Array{<:Real}
    return [first(regressor.model(x)) for x in X]
end


function MetricRegressors.getnumparams(regressor::NeuralNetRegressor)::Integer
    return 0    # Not implemented
end
#endregion neuralnet
end
