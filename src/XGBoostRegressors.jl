#=
 * File: XGBoostRegressors.jl
 * Project: src
 * File Created: Monday, 11th November 2019 6:42:36 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 27th January 2020 9:19:46 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module XGBoostRegressors

using XGBoost

using MetricTools
using ..MetricRegressors

export XGBoostRegressor

mutable struct XGBoostRegressor <: Regressor
    bst::Booster
    epochs::Integer
    eta::Integer
    maxdepth::Integer
    XGBoostRegressor(; epochs=10::Integer, eta=1::Integer, maxdepth=2::Integer) =
        new(Booster(), epochs, eta, maxdepth)
end

function MetricRegressors.train!(regressor::XGBoostRegressor, X::Array{Point}, Y::Array{<:Real})
    X1::Matrix{Float64} = hcat(X...)'
    regressor.bst = xgboost(X1, regressor.epochs;
        label=Y, eta=regressor.eta, max_depth=regressor.maxdepth)
end

function MetricRegressors.predict(regressor::XGBoostRegressor, X::Array{Point})::Array{<:Real}
    X1::Matrix{Float64} = hcat(X...)'
    return XGBoost.predict(regressor.bst, X1)
end

function MetricRegressors.getnumparams(regressor::XGBoostRegressor)::Integer
    regressor.epochs * (2^(regressor.maxdepth + 1) - 1)
end
end
