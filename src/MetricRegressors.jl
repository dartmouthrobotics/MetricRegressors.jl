#=
 * File: MetricRegressors.jl
 * Project: src
 * File Created: Monday, 11th November 2019 6:39:42 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 4th February 2020 10:47:59 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module MetricRegressors

using MetricTools

export
    Regressor,
    LinearRegressor,
    GaussianRegressor,
    NeuralNetRegressor,
    XGBoostRegressor,
    train!,
    predict,
    getnumparams,
    mse,
    tse

"""
Abstract type for a Regressor
Should all implement:
    train!(Regressor, Array{Point})
    predict(Regressor, Array{Point})
"""
abstract type Regressor end

"""
Methods to be implemented
"""
@mustimplement train!(regressor::Regressor, X::Array{Point}, Y::Array{<:Real})
@mustimplement predict(regressor::Regressor, X::Array{Point})::Array{<:Real}
@mustimplement getnumparams(regressor::Regressor)::Integer

"""
Auxilliary overloads
"""
train!(regressor::Regressor, x::Point, y::Real) =
    train!(regressor, Point[x], Real[y])
predict(regressor::Regressor, x::Point) =
    first(predict(regressor, Point[x]))

"""
Calculates the total squared error of data for a regressor
"""
function tse(regressor::Regressor, X::Array{Point}, Y::Array{<:Real})::Real
    totalerr::Real = 0
    Yt::Array{Union{<:Real, Nothing}} = predict(regressor, X)
    for i in eachindex(Y)
        if isnothing(Yt[i]) || isnothing(Y[i]) continue end 
        totalerr += (Yt[i] - Y[i])^2
    end

    return totalerr
end

"""
Calculates mean squared error
"""
@inline mse(regressor::Regressor, X::Array{Point}, Y::Array{<:Real}) =
    tse(regressor, X, Y) / length(Y)

"""
Gets the distance function for the regressor
"""
MetricTools.distfunc(regressor::Regressor)::Function = 
    (x::Point) -> predict(regressor, x)

include("./LinearRegressors.jl")
include("./GaussianRegressors.jl")
include("./XGBoostRegressors.jl")
include("./NeuralNetRegressors.jl")

using
    .LinearRegressors,
    .GaussianRegressors,
    .XGBoostRegressors,
    .NeuralNetRegressors

end # module
