#=
 * File: regressors.jl
 * Project: common
 * File Created: Wednesday, 2nd October 2019 12:13:00 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 21st December 2019 10:50:21 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import JLD

using MetricTools

if false
    include("../src/MetricRegressors.jl")
    using
        .MetricRegressors
else
    using
        MetricRegressors
end
#endregion imports

function l2origin(point::Point)::Real
    origin::Point = [0, 0]
    return sqrt(sum((point - origin).^2))
end

function test_regressors()
    println("Testing regressors...")
    n::Integer = 100
    points::Array{Point} = [rand(2) for i in 1:n]
    values::Array{<:Real} = [l2origin(p) for p in points]
    testpoints::Array{Point} = [
        [0.1, 0.1],
        [0.0, 0.0],
        [0.5, 0.5]
    ]
    
    regressors::Array{Regressor} = [
        LinearRegressor(),
        GaussianRegressor(),
        NeuralNetRegressor(; epochs=100),
        XGBoostRegressor(; epochs=20, maxdepth=3)
    ]

    for regressor in regressors
        println("Regressor: $(typeof(regressor))")
        println("    Training with $(length(points)) points and $(length(values)) values...")
        train!(regressor, points, values)
        
        println("    Predicting values...")
        testvalues = predict(regressor, testpoints)
        for (testpoint, value) in zip(testpoints, testvalues)
            println("        $testpoint: $value")
        end

        if regressor isa GaussianRegressor
            dir::String = "./data/output/models/"
            mkpath(dir)
            file::String = "gp1.jld"
            
            println("    Saving to $dir$file...")
            JLD.save(dir * file, "gp", regressor.gp)
            println("    Saved.")
        end

        df::Function = distfunc(regressor)
        val = df(first(testpoints))
    end
    return true
end

test_regressors()

# @testset "Regressors Tests" begin
#     @test test_regressors()
# end
