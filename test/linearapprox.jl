#=
 * File: linearapprox.jl
 * Project: test
 * File Created: Monday, 11th November 2019 7:02:06 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 11th November 2019 7:39:16 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using LinearAlgebra
using MetricTools

function getmatrix1(points)
    getv = (i) -> (p) -> (p[i])
    vals = [getv(i).(points) for i in 1:length(first(points))]
    matrix = hcat(fill(1, length(points)), vals...)
    return matrix
end

function getmatrix2(P)
    return hcat([[1; p] for p in P]...)'
end

function getmatrix3(P)
    N, D = length(P), length(first(P))
    M::Matrix = fill(1., (N, D+1))
    for n in 1:N
        for d in 1:D
            M[n, d+1] = P[n][d]
        end
    end
    return M
end

"""
Constructs a linear approximation for list of points and their values
"""
function linearapprox(points::Array{Point}, rhs::Array{<:Real})::Array{<:Real}
    @time matrix = getmatrix1(points)
    @time matrix = getmatrix1(points)
    println()
    
    @time matrix = getmatrix2(points)
    @time matrix = getmatrix2(points)
    println()

    @time matrix = getmatrix3(points)
    @time matrix = getmatrix3(points)
    println()
    
    return matrix \ rhs
end

f(p) = sqrt(p[1]^2 + p[2]^2)

function test_linearapprox()
    P::Array{Point} = [
        rand(2) for i in 1:100000
    ]
    coeff = linearapprox(P, f.(P))
end

function fastmat()
    P::Array{Point} = [
        [0, 0],
        [1, 2],
        [2, 3],
        [1, 0],
    ]

    # M = hcat(P...)'
    M = getmatrix3(P)
    println(M)
end

test_linearapprox()

# fastmat()
